package com.devcamp.country_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryCrudApplication.class, args);
	}

}
