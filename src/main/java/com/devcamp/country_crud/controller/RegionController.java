package com.devcamp.country_crud.controller;

import com.devcamp.country_crud.model.CCountry;
import com.devcamp.country_crud.model.CRegion;
import com.devcamp.country_crud.repository.CountryRepository;
import com.devcamp.country_crud.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@CrossOrigin
	@PostMapping("/country/{countryId}/regions")
	public ResponseEntity<Object> createRegion(@PathVariable("countryId") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
			CRegion newRole = new CRegion();
			if (cRegion.getRegionCode() != null) {
				newRole.setRegionCode(cRegion.getRegionCode());
			}
			if (cRegion.getRegionName() != null) {
				newRole.setRegionName(cRegion.getRegionName());
			}
			if (cRegion.getCountry() != null) {
				newRole.setCountry(cRegion.getCountry());
			}
			CCountry _country = countryData.get();
			newRole.setCountry(_country);
			newRole.setCountryName(_country.getCountryName());
			
			CRegion savedRole = regionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified region: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/region/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/region/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/region/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}


	@CrossOrigin
	@GetMapping("/region")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
    public List < CRegion > getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "id") Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId,countryId);
    }

	@CrossOrigin
	@GetMapping("/country/{countryId}/region-count")
	public ResponseEntity<Object> countRegionsByCountryId(@PathVariable Long countryId) {
		Optional<CCountry> countryData = countryRepository.findById(countryId);
		if (countryData.isPresent()) {
			int regionCount = regionRepository.countByCountryId(countryId);
			return ResponseEntity.ok(regionCount);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	
	@CrossOrigin
	@GetMapping("/region/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}
}

