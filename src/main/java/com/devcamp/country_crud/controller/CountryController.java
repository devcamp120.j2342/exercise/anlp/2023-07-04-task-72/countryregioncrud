package com.devcamp.country_crud.controller;

import com.devcamp.country_crud.model.CCountry;
import com.devcamp.country_crud.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/country")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified country: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/country/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			if (cCountry.getCountryCode() != null) {
				newCountry.setCountryCode(cCountry.getCountryCode());
			}
			if (cCountry.getCountryName() != null) {
				newCountry.setCountryName(cCountry.getCountryName());
			}
			if (cCountry.getRegions() != null) {
				newCountry.setRegions(cCountry.getRegions());
			}
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/country/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional = countryRepository.findById(id);
			if (optional.isPresent()) {
				countryRepository.deleteById(id);
			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/country")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country-count")
	public long countCountry() {
		return countryRepository.count();
	}

	@CrossOrigin
	@GetMapping("/country/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}

	@GetMapping("/country5")
	public ResponseEntity<List<CCountry>> getFiveVoucher(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			List<CCountry> list = new ArrayList<CCountry>();
			countryRepository.findAll(pageWithFiveElements).forEach(list::add);
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return null;
		}
	}

}
